import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  serverPath = 'http://interview.agileengine.com/';
  jwt = '';
  constructor(
    private http: HttpClient,
  ) {
  }

  getPhoto(id: string) {
    const url = `${this.serverPath}images/${id}`;
    const headers = {
      'Authorization': this.jwt,
    };

    return this.http.get(url, {headers});
  }

  getPhotos(page: number = 1) {
    const url = `${this.serverPath}images?page=${page}`;
    const headers = {
      'Authorization': this.jwt,
    };

    return this.http.get(url, {headers});
  }

  getBearer() {
    // console.log('get berarer');
    const url = `${this.serverPath}auth`;
    const body = { 
      'apiKey': '23567b218376f79d9415' 
    }
    return new Promise((resolve, reject) => {
      this.http.post(url, body).subscribe(response => {
        if (response['auth']) {
          this.jwt = `Bearer ${response['token']}`;
        } else {
          this.jwt = '';
        }
        resolve(response);
      }, error => {
        // console.log('error', error);
        this.jwt = '';
        reject(error);
      }
    )});
  }
}

export interface Token {
  value: string,
}