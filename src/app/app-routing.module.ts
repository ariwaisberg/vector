import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GalleryComponent } from 'src/components/gallery/gallery.component';
import { PhotoComponent } from 'src/components/photo/photo.component';

const routes: Routes = [
  { path: 'photo/:id', component: PhotoComponent},
  { path: '**', component: GalleryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
