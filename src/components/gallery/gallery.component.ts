import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/services/api/api.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  images: any[]=[];
  currentPage = 1;
  totalPages = 1;

  constructor(
    private router: Router,
    private api: ApiService, 
  ) { }

  ngOnInit(): void {
    const bearer = this.api.getBearer().then(result => {
      this.getPhotos();
    }).catch(error => {
      console.log(error)
    })
  }

  clickImage(image: any) {
    this.router.navigateByUrl(`/photo/${image.id}`);
  }

  getPhotos() {
    this.api.getPhotos(this.currentPage).subscribe(response => {
      this.images = response['pictures'];
      this.totalPages = response['pageCount'];
    })
  }

  btnNext() {
    if (this.currentPage === this.totalPages) {
      console.log('No more pages!');
    } else {
      this.currentPage++;
      this.getPhotos();
    }
  }

  btnPrev() {
    if (this.currentPage === 1) {
      console.log('No previous pages!');
    } else {
      this.currentPage = this.currentPage -1;
      this.getPhotos();
    }
  }

}
