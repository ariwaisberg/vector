import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/services/api/api.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {
  image: any;

  constructor(
    private api: ApiService,
    private route: ActivatedRoute,
  ) { 
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.api.getPhoto(id).subscribe(response => {
        this.image = response;
      }, error => {
        console.log(error);
      })
    })
  }
}